# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 19:00:12 2019

@author: CLAB
"""

import psychopy
import psychopy.visual as visual
import psychopy.event

class stereo_display():
    def __init__(self,nscreenL=0,nscreenR=1):
        self.nscreenL=nscreenL
        self.nscreenR=nscreenR
        return None

    def draw(self,widgets=[],which_side='both',doflip=True,flipclear=True):
        if which_side=='both' or which_side=='left':
            self.draw1(widgets,'left',doflip,flipclear)
        if which_side=='both' or which_side=='right':
            self.draw1(widgets,'right',doflip,flipclear)
    
    def setup_fixation(self,fixation_left,fixation_right):
        self.fixation_left=fixation_left
        self.fixation_right=fixation_right
        
    def draw_fixation(self,doflip=True):
        self.draw1(self.fixation_left,'left',doflip=doflip)
        self.draw1(self.fixation_right,'right',doflip=doflip)
                
class test_mono(stereo_display):
    # This is not a true stereo display, just used to test things.
    def draw1(self,widgets,which_side,doflip=True):
        #if which_side=='right':
            #return    
        win=self.winL
        for widget1 in widgets:
            widget1.draw()
        if doflip:
            win.flip()
    def setup_win(self,**winparams):
        self.winL = psychopy.visual.Window(screen=self.nscreenL,**winparams)
        return self.winL,self.winL
    def __del__(self):
        self.winL.close()
    
class Nvidia(stereo_display):
    def draw1(self,widgets,which_side,doflip=True):
        win=self.winL
        win.setBuffer(which_side)
        for widget1 in widgets:
            widget1.draw()
        if which_side=='right' and doflip:
            win.flip()
    def setup_win(self,**winparams):
        self.winL = psychopy.visual.Window(screen=self.nscreenL,**winparams)
        return self.winL,self.winL
    def __del__(self):
        self.winL.close()
         
class haploscope(stereo_display):
    def setup_win(self,**winparams):
        self.winL = psychopy.visual.Window(screen=self.nscreenL,**winparams)
        self.winR = psychopy.visual.Window(screen=self.nscreenR,**winparams)
        return self.winL,self.winR

    def draw1(self,widgets,which_side,doflip=True):
        if which_side=='left':
            win=self.winL
        else:
            win=self.winR
        for widget1 in widgets:
            widget1.draw()
        if doflip:
            win.flip()

    def __del__(self):
        self.winL.close()
        self.winR.close()
         
class PROPixx(stereo_display):
    def setup_win(self,**winparams):
        self.winL = psychopy.visual.Window(screen=self.nscreenL,**winparams)
        win=self.winL

        from pypixxlib.propixx import PROPixx, PROPixxCTRL
        from pypixxlib._libdpx import DPxSelectDevice

        self.my_device = PROPixx()
        self.my_device.setRearProjectionMode(True)
        self.my_device.setVideoVesaBlueline(True)
        self.my_device.updateRegisterCache()

        # These widgets are for the "blue line" at the bottom of the screen that is used to signal stereo
        # to the ProPixx
        dims=win.size
        wid=dims[0]/4.0      
        self.lefton =visual.Line(win=win,start=(-dims[0]//2,-dims[1]//2.0), end=(-dims[0]//2+wid ,-dims[1]//2.0), lineColor=[0,0,1], lineWidth=10 )  
        self.righton=visual.Line(win=win,start=(-dims[0]//2+wid,-dims[1]//2.0), end=(dims[0]//2 ,-dims[1]//2.0), lineColor=[0,0,1], lineWidth=10 )  
        self.leftoff =visual.Line(win=win,start=(-dims[0]//2,-dims[1]//2.0), end=(-dims[0]//2+wid ,-dims[1]//2.0), lineColor=[0,0,0], lineWidth=10 )  
        self.rightoff=visual.Line(win=win,start=(-dims[0]//2+wid,-dims[1]//2.0), end=(dims[0]//2 ,-dims[1]//2.0), lineColor=[0,0,0], lineWidth=10 )  
        return self.winL,self.winL

    def draw1(self,widgets,which_side,doflip=True,flipclear=True):
        win=self.winL
        for widget1 in widgets:
            widget1.draw()
        if which_side=='left':
            #self.rightoff.draw()
            self.lefton.draw()
        elif which_side=='right':
            #self.leftoff.draw()
            self.righton.draw()
        if doflip:
            win.flip() #clearBuffer=flipclear)
            
    def stereo_waitKeys(self,widgets_l,widgets_r,keyList=None):
        """ Don't want to do a waitKeys on the PROPixx, since it kills the stereo interleaving.
            Instead, flip each side and poll for keys.
            
            - If widget_l and widget_r are passed in, draw those each time (and clear buffer)
            
            - Otherwise, don't do a clear buffer when flipping, to keep previously drawn thing
            (instructions, etc.) on display
            
            - The non-clear is problematic for the PROPixx (bluelines stack up), so better to pass
              in the message using the widgets.
            """
            
        done=False
        while done==False:
            flipclear=True #len(widgets_l)>0
            self.draw1(widgets_l,'left', doflip=True,flipclear=flipclear)
            self.draw1(widgets_r,'right',doflip=True,flipclear=flipclear)
            keys=psychopy.event.getKeys(keyList)
            if len(keys)>0:
                return keys
            
    def __del__(self):
        self.winL.close()
        self.my_device.close()

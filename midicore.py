# -*- coding: utf-8 -*-
"""
Created on Fri Sep  7 17:24:35 2018

@author: CLAB
"""

from rtmidi.midiutil import open_midiinput

MIDI_max=127
MIDI_btn_push=127
MIDI_btn_off=0

# "Novation Launch Control" specifics
LC_1_top=21
LC_2_top=22
LC_3_top=23
LC_4_top=24
LC_5_top=25
LC_6_top=26
LC_7_top=27
LC_8_top=28
LC_1_btm=41
LC_2_btm=42
LC_3_btm=43
LC_4_btm=44
LC_5_btm=45
LC_6_btm=46
LC_7_btm=47
LC_8_btm=48

LC_btn_1= 9
LC_btn_2=10
LC_btn_3=11
LC_btn_4=12
LC_btn_5=25
LC_btn_6=26
LC_btn_7=27
LC_btn_8=28


class midi_control:
    def __init__(self):
        self.midiin = None      
        
    def init_midi(self,port=0):
        try:
            self.midiin,port_name = open_midiinput(port)
            print("Found %s"%port_name)
        except IOError:
            print("No MIDI found. It should be okay.")

    def poll_knobs(self):
        # Novation Launch Control
        
        # Knobs:
        # top_row: 21-28
        # bottom_row: 41-48

        # Buttons: 9-12, then 25-28
        
        if self.midiin==None:
            return None

        msg=self.midiin.get_message()
        if msg:

            msg,tim=msg
            if msg[0]==184: # CC
                which_CC=msg[1]
                value=msg[2]
                #print which_CC,value
                return which_CC,value
            elif msg[0]==152: # note on, value should be 127
                #print msg
                return msg[1],msg[2]
            elif msg[0]==136: # note off, value should be 0
                return msg[1],msg[2]
                
            return None
        else: # no msg in queue
            return None 

    def close(self):
        if not self.midiin is None:
            self.midiin.close_port()    
        
if __name__=="__main__":
    import time
    demo=midi_control()
    demo.init_midi()
    
    while True:
        msg=demo.poll_knobs()
        time.sleep(0.1)
    
    demo.close()
    